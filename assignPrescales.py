#!/usr/bin/env python

'''
Usage: ./assignPrescales.py file1.json file2.json

Creates a new HLT prescale json file MyPrescales.json in the current workdir
with the set of chains identical as file1.json and prescale values copied from file2.json.

For every chain that exists in both file1.json and file2.json, the prescale value from file2.json is taken.
For every chain that exists only in file1.json, the value from file1.json is taken.
Chains that only exist in file2.json are skipped.
'''

import sys
import json

currentFile = sys.argv[1]
otherFile = sys.argv[2]

with open(currentFile,'r') as f:
  currentPS = json.load(f)

with open(otherFile,'r') as f:
  otherPS = json.load(f)

newPrescales = dict()

for chain,chainPS in currentPS['prescales'].items():
  newPrescales[chain] = chainPS
  if chain not in otherPS['prescales']:
    newPrescales[chain]['enabled'] = False
    newPrescales[chain]['prescale'] = -1.0
    if 'enabled_express' in chainPS:
      newPrescales[chain]['enabled_express'] = False
      newPrescales[chain]['prescale_express'] = -1.0
    continue
  
  otherChainPS = otherPS['prescales'][chain]
  newPrescales[chain]['enabled'] = otherChainPS['enabled']
  newPrescales[chain]['prescale'] = otherChainPS['prescale']
  if 'enabled_express' in chainPS:
    if 'enabled_express' in otherChainPS:
      newPrescales[chain]['enabled_express'] = otherChainPS['enabled_express']
      newPrescales[chain]['prescale_express'] = otherChainPS['prescale_express']

currentPS['prescales'] = newPrescales

with open('MyPrescales.json','w') as f:
  json.dump(currentPS,f,indent=4)
