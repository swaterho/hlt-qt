NFORKS=$1            #number of forks on a TPU (--nproc in athenaHLT)
NTHREADS=$2        #number of threads in each fork (--nthreads)
NSLOTS=$3  #number of event slots in each fork (--concurrent-events)
NEVENTS=10000
SCRIPTSDIR=/afs/cern.ch/user/s/swaterho/QT/dev/scripts

#echo "Copying configuration files"
#cp ../config/*.json .
#cp ../../test/AthHLT.sor.pkl .
date
hostname
echo "Running athenaHLT with ${NFORKS}-${NTHREADS}-${NSLOTS} config"
vtune -collect hotspots -strategy=:trace:trace,ld-linux.so.2:notrace:notrace,ld-2.12.so:notrace:notrace,ld-linux.so:notrace:notrace,ld-linux-x86-64.so.2:notrace:notrace -- \
athena.py -c \
"setMenu='Dev_pp_run3_v1_TriggerValidation_prescale';doL1Sim=False;doWriteBS=False;doWriteRDOTrigger=True;fpeAuditor=True;" \
--imf --pmon=perfmonmt \
--threads=${NTHREADS}  --concurrent-events=${NSLOTS} --evtMax=${NEVENTS} \
--filesInput=/cvmfs/atlas-nightlies.cern.ch/repo/data/data-art/TrigP1Test/data22_13p6TeV.00431885.physics_EnhancedBias.merge.RAW._lb0545._SFO-15._0001.1,/cvmfs/atlas-nightlies.cern.ch/repo/data/data-art/TrigP1Test/data22_13p6TeV.00431885.physics_EnhancedBias.merge.RAW._lb0545._SFO-17._0001.1 \
TriggerJobOpts/runHLT_standalone.py \

>athena.log 2>&1 


echo "Finished, running post-processing for ${NFORKS}-${NTHREADS}-${NSLOTS} config"
# ${SCRIPTSDIR}/extract-times.sh
# ${SCRIPTSDIR}/histmerge.sh >histmerge.log 2>&1

#echo "Finished post-processing for ${NFORKS}-${NTHREADS}-${NSLOTS} config"     