#!/bin/bash
#
# Script analysing athenaHLT.log to find the start and end time of the event loop,
# relative to the start time of the job. The timestamps are written to a file and
# printed to stdout. The numbers are given in seconds.
#

intdiff () {
  echo "$(($1 - $2))"
}

epochdate () {
  echo "$(date -d "$1" +%s)"
}
# Requires adjusting the selected timestamps column in the t_start command between 4 and 5, depending on whether the current day of the 
# month is one-digit or two-digit (because of an extra space in the former case).
t_start=$(head -n 1 athenaHLT.log | cut -d ' ' -f 5)

t_loop_start=$(grep 'HltEventLoopMgr.*Starting loop' -R athenaHLT*.out | cut -d ' ' -f 2 | sort -n | head -n 1 | cut -d ',' -f 1)

t_loop_end=$(grep 'HltEventLoopMgr.*All events processed' -R athenaHLT*.out | cut -d ' ' -f 2 | sort -n | tail -n 1 | cut -d ',' -f 1)

echo "t_start      ${t_start}"
echo "t_loop_start ${t_loop_start}"
echo "t_loop_end   ${t_loop_end}"

e_start=$(epochdate $t_start)
e_loop_start=$(epochdate $t_loop_start)
e_loop_end=$(epochdate $t_loop_end)

echo $(intdiff $e_loop_start $e_start) > timestamps.txt
echo $(intdiff $e_loop_end $e_start) >> timestamps.txt

cat timestamps.txt

