#!/bin/bash

# Names in order of how they are printed for Python convenience
names=('ten' 'one' 'twelve' 'fourteen' 'sixteen' 'eighteen' 'twenty' 'two' 'twentytwo' 'twentyfour' 'twentysix' 'twentyeight' 'thirty' 'thirtytwo' 'four' 'six' 'eight')
#names=('ten' 'one' 'twelve' 'fourteen' 'sixteen' 'eighteen' 'twenty' 'two' 'twentytwo' 'twentyfour' 'twentysix' 'twentyeight' 'thirty' 'thirtytwo' 'thrityfour' 'thirtysix' 'thirtyeight' 'forty' 'fortytwo' 'four' 'fortyfour' 'fortysix' 'fortyeight' 'fifty' 'fiftytwo' 'fiftyfour' 'fiftysix' 'fiftyeight' 'sixty' 'sixtytwo' 'sixtyfour' 'six' 'eight')
#names=('four')
#names=('one' 'two' 'four')
# Define counter to keep track of where in the names we are 
COUNTER=-1 

for dir in */ ; do
    cd $dir
    let COUNTER++
# Unpack perfmon report
    #tar -xf perfmonmt.json.tar.gz
# Number of events in run
    events=$(grep 'INFO Number of events processed' athena.log | tr -s ' ' | cut -d ' ' -f 7)

# Define VTune parameters
    phys_core_util=$(grep 'Effective Physical Core Utilization' athena.log | cut -d ' ' -f 5 | cut -d '%' -f 1)
    logi_core_util=$(grep 'Effective Logical Core Utilization' athena.log | cut -d ' ' -f 9 | cut -d '%' -f 1)
    effective_time_vtune=$(grep 'Effective Time' athena.log | cut -d ' ' -f 11 | cut -d 's' -f 1)
    spin_time=$(grep 'Spin Time' athena.log | cut -d ' ' -f 11 | cut -d 's' -f 1)
    elapsed_time=$(sed -n -e 's/^.*Elapsed Time: //p' athena.log | cut -d 's' -f 1)
#   cpu_per_event=$(grep 'CPU usage per event' athena.log | tr -s ' ' | cut -d ' ' -f 8)

# Define PerfMonMT parameters
    #cpuUtilEff=$(grep 'cpuUtilEff' perfmonmt.json | cut -d ' ' -f 14)
    eventsPS=$(grep 'Events per second' athena.log | tr -s ' ' | cut -d ' ' -f 6)
    effective_time_PM=$(grep 'INFO Execute' athena.log | tr -s ' ' | cut -d ' ' -f 4)
    drss=$(grep 'INFO Execute' athena.log | tr -s ' ' | cut -d ' ' -f 8)
    #echo ${names[$COUNTER]} = "np.array([$phys_core_util, $logi_core_util, $effective_time_vtune, $spin_time, 0 , $eventsPS, $effective_time_PM, $drss, $elapsed_time])"
    echo ${names[$COUNTER]} = "np.array([$eventsPS])"
    cd ..
done