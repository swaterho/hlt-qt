# This will iterate through the config only jobs and replace the L1 menu and prescale with the correct one for the data
for file in */; do
cd $file
rm L1Menu_Dev_pp_run3_v1_TriggerValidation_prescale_23.0.20.json L1PrescalesSet_Dev_pp_run3_v1_TriggerValidation_prescale_23.0.20.json
cp /eos/user/s/swaterho/vtune_test/Run_5_pickletest_wrongmenu/pickle_configs/L1Menu_Dev_pp_run3_v1_TriggerValidation_prescale_23.0.20.json .
cp /eos/user/s/swaterho/vtune_test/Run_5_pickletest_wrongmenu/pickle_configs/L1PrescalesSet_Dev_pp_run3_v1_TriggerValidation_prescale_23.0.20.json .
# mv L1Menu_Dev_pp_run3_v1_TriggerValidation_prescale_23.0.18.json L1Menu_Dev_pp_run3_v1_TriggerValidation_prescale_23.0.20.json
# mv L1PrescalesSet_Dev_pp_run3_v1_TriggerValidation_prescale_23.0.18.json L1PrescalesSet_Dev_pp_run3_v1_TriggerValidation_prescale_23.0.20.json
cd ../
done