# Other imports and script setup
import os
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
from atlasify import atlasify
import numpy as np

# Specify directories
data_dirs = ["/eos/user/s/swaterho/vtune_test/Run_20_FxT1Sx_smk3154_r2201010_athHLT_pkl_lb497",
             "/eos/user/s/swaterho/vtune_test/Run_25_newMenu_FxT1Sx__smk3154_r2201010_athHLT_pkl_lb497"]  

colors = ['r', 'b', 'b', 'y']  
markers = ['o', 'v', '^', 's', '8', 's', 'p', '*', 'h', 'H', 'D', 'd', 'P', 'X']
labels = ['Old Menu', 'New Menu', 'Hybrid, 2 forks', 'Hybrid, 4 forks']


for i, data_dir in enumerate(data_dirs):
    x = []
    y = []

    # Change working directory to the current dataset directory
    os.chdir(data_dir)

    for dir in os.listdir("."):
        if not os.path.isdir(dir):
            continue

        if labels[i] == 'Multi-processing': 
            x_value = int(dir.split("-")[0]) 
        else:
            x_value = int(dir.split("-")[0])

        # Skip odd x_value, but keep x_value = 1
        if x_value != 1 and x_value % 2 != 0:
            continue

        os.chdir(dir)

        if os.path.isfile("timestamps.txt"):
            with open("timestamps.txt", "r") as file:
                timestamps = [int(line.strip()) for line in file.readlines()[:2]]

            try:
                time_diff = 19210 / (timestamps[1] - timestamps[0])
                if time_diff < 0:
                    time_diff = 19210 / ((timestamps[1] + 86400) - timestamps[0])
            except ZeroDivisionError:
                time_diff = 0

            x.append(x_value)
            y.append(float(time_diff))

        os.chdir("..")

    plt.scatter(x, y, edgecolors=colors[i], marker=markers[i], label=labels[i], s=50, facecolors='none')

    os.chdir("..")


ax = plt.gca()

ax.set_xlabel("Number of events processed in parallel", loc='center', fontsize=15, labelpad=8)
ax.set_ylabel("Throughput [events / s]", loc='center', fontsize=15, labelpad=12)
ax.set_xlim(0,25)
ax.set_ylim(0,50)
ax.grid(axis='both', which='major', linestyle='-', alpha=0.7)
ax.grid(axis='both', which='minor', linestyle='-', alpha=0.3)
ax.xaxis.set_major_locator(ticker.MultipleLocator(2))
ax.yaxis.set_major_locator(ticker.MultipleLocator(10.0))

ax.xaxis.set_minor_locator(ticker.MultipleLocator(4))
ax.yaxis.set_minor_locator(ticker.MultipleLocator(5.0))

ax.tick_params(axis='both', which='major', labelsize=14)

ax.set_title('Pure Multiprocessing Throughput (forks=slots, 1 thread)')

atlasify('Internal', 'Data, <$\mu$> $=$54')

plot_file = os.path.join(os.getcwd(), "plot.png")
plt.savefig(plot_file)

plt.close()

print("Plot saved as plot.png")

