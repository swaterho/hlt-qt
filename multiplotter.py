import os
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.ticker import (MultipleLocator, AutoMinorLocator)

SCRIPTSDIR = "/afs/cern.ch/user/s/swaterho/QT/scripts"

# Define the directories with your different data sets
data_dirs = ["/eos/user/s/swaterho/vtune_test/Run_16_MT_smk3154_r220101_athHLT_pkl_lb497",
             "/eos/user/s/swaterho/vtune_test/Run_17_MP_smk3154_r220101_athHLT_pkl_lb497", 
             "/eos/user/s/swaterho/vtune_test/Run_18_mP2_mTx_smk3154_r2201010_athHLT_pkl_lb497", 
             "/eos/user/s/swaterho/vtune_test/Run_19_mP4_mTx_smk3154_r2201010_athHLT_pkl_lb497"]  

# Set up plot
plt.figure(figsize=(10, 6))
colors = ['r', 'g', 'b', 'y']  
markers = ['o', 'v', '^', 's', '8', 's', 'p', '*', 'h', 'H', 'D', 'd', 'P', 'X']
labels = ['Multi-threading', 'Multi-processing', 'Hybrid, 2 forks', 'Hybrid, 4 forks']

for i, data_dir in enumerate(data_dirs):
    x = []
    y = []

    # Change working directory to the current dataset directory
    os.chdir(data_dir)

    # Loop over all subdirectories, extracts the times and plots them
    for dir in os.listdir("."):
        if not os.path.isdir(dir):
            continue

        # Extract the 'x' value from the directory name
        if labels[i] == 'Multi-processing': 
            x_value = dir.split("-")[0] 
        else:
            x_value = dir.split("-")[1]

        # Change to the current directory
        os.chdir(dir)

        # Check if timestamps.txt exists in this directory
        if os.path.isfile("timestamps.txt"):
            with open("timestamps.txt", "r") as file:
                # Read the first two lines of timestamps.txt
                timestamps = [int(line.strip()) for line in file.readlines()[:2]]

            # Calculate the throughput via (number of events)/(execution time), handling ZeroDivisionError
            try:
                time_diff = 19210 / (timestamps[1] - timestamps[0])
                #print(time_diff)
                if time_diff < 0:
                    time_diff = 19210 / ((timestamps[1] + 86400) - timestamps[0])
            except ZeroDivisionError:
                time_diff = 0

            x.append(int(x_value))
            y.append(float(time_diff))

        # Change back to the dataset directory
        os.chdir("..")

    # Plot the time differences using Matplotlib for current dataset
    plt.scatter(x, y, edgecolors=colors[i], marker=markers[i], label=labels[i], s=50, facecolors='none')  # Updated scatter function

    # Change back to the parent directory
    os.chdir("..")

# Get current axes
ax = plt.gca()

# Set the labels and title
plt.xlabel("Number of slots/threads or forks (for pure MP)")
plt.ylabel("Throughput [events / s]")
plt.title("AthenaHLT Throughput (22.0.101, patched, releaseMenu, ~20k events)")
plt.xlim(0, 50)  
plt.ylim(0, 80)

# Major and minor ticks
ax.xaxis.set_major_locator(MultipleLocator(8))
ax.xaxis.set_minor_locator(MultipleLocator(4))
ax.yaxis.set_major_locator(MultipleLocator(10))
ax.yaxis.set_minor_locator(MultipleLocator(5))

plt.grid(True, which='both', axis='both', alpha=0.5)  # Set grid transparency to 50%
ax.set_axisbelow(False)  # Draw markers on top of the grid
plt.minorticks_on()
plt.legend()

# Save the plot to a file
plot_file = os.path.join(os.getcwd(), "plot.png")
plt.savefig(plot_file)

plt.close()

print("Plot saved as plot.png")
