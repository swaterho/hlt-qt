NFORKS=$1            #number of forks on a TPU (--nproc in athenaHLT)
NTHREADS=$2        #number of threads in each fork (--nthreads)
NSLOTS=$3  #number of event slots in each fork (--concurrent-events)
#NEVENTS=10  --evtMax=${NEVENTS} 
SCRIPTSDIR=/afs/cern.ch/user/s/swaterho/QT/dev/scripts

#echo "Copying configuration files"
#cp ../config/*.json .
#cp ../../test/AthHLT.sor.pkl .
date
hostname
echo "Running athenaHLT with ${NFORKS}-${NTHREADS}-${NSLOTS} config"
#vtune -collect hotspots -strategy=:trace:trace,ld-linux.so.2:notrace:notrace,ld-2.12.so:notrace:notrace,ld-linux.so:notrace:notrace,ld-linux-x86-64.so.2:notrace:notrace -- \
athenaHLT.py \
--threads=${NTHREADS} --concurrent-events=${NSLOTS} \
-c "setMenu='PhysicsP1_pp_run3_v1_Primary_prescale';setDetDescr='ATLAS-R3S-2021-03-00-00';setGlobalTag='CONDBR2-HLTP-2022-02'; condOverride = {'/PIXEL/Onl/ChargeCalibration' : 'ChargeCalibrationOnl-HLT-UPD1-000'}; from MuonRecExample.MuonRecFlags import muonRecFlags;muonRecFlags.runCommissioningChain.set_Value_and_Lock(True);from AthenaConfiguration.AllConfigFlags import ConfigFlags;ConfigFlags.Muon.runCommissioningChain=True;" \
-f /eos/user/s/swaterho/data22_13p6TeV/physics_EnhancedBias/00440499/data22_13p6TeV.00440499.physics_EnhancedBias.merge.RAW/data22_13p6TeV.00440499.physics_EnhancedBias.merge.RAW._lb0497._SFO-11._0001.1 \
-f /eos/user/s/swaterho/data22_13p6TeV/physics_EnhancedBias/00440499/data22_13p6TeV.00440499.physics_EnhancedBias.merge.RAW/data22_13p6TeV.00440499.physics_EnhancedBias.merge.RAW._lb0497._SFO-12._0001.1 \
-f /eos/user/s/swaterho/data22_13p6TeV/physics_EnhancedBias/00440499/data22_13p6TeV.00440499.physics_EnhancedBias.merge.RAW/data22_13p6TeV.00440499.physics_EnhancedBias.merge.RAW._lb0497._SFO-13._0001.1 \
-f /eos/user/s/swaterho/data22_13p6TeV/physics_EnhancedBias/00440499/data22_13p6TeV.00440499.physics_EnhancedBias.merge.RAW/data22_13p6TeV.00440499.physics_EnhancedBias.merge.RAW._lb0497._SFO-14._0001.1 \
-f /eos/user/s/swaterho/data22_13p6TeV/physics_EnhancedBias/00440499/data22_13p6TeV.00440499.physics_EnhancedBias.merge.RAW/data22_13p6TeV.00440499.physics_EnhancedBias.merge.RAW._lb0497._SFO-15._0001.1 \
-f /eos/user/s/swaterho/data22_13p6TeV/physics_EnhancedBias/00440499/data22_13p6TeV.00440499.physics_EnhancedBias.merge.RAW/data22_13p6TeV.00440499.physics_EnhancedBias.merge.RAW._lb0497._SFO-16._0001.1 \
-f /eos/user/s/swaterho/data22_13p6TeV/physics_EnhancedBias/00440499/data22_13p6TeV.00440499.physics_EnhancedBias.merge.RAW/data22_13p6TeV.00440499.physics_EnhancedBias.merge.RAW._lb0497._SFO-17._0001.1 \
-f /eos/user/s/swaterho/data22_13p6TeV/physics_EnhancedBias/00440499/data22_13p6TeV.00440499.physics_EnhancedBias.merge.RAW/data22_13p6TeV.00440499.physics_EnhancedBias.merge.RAW._lb0497._SFO-18._0001.1 \
-f /eos/user/s/swaterho/data22_13p6TeV/physics_EnhancedBias/00440499/data22_13p6TeV.00440499.physics_EnhancedBias.merge.RAW/data22_13p6TeV.00440499.physics_EnhancedBias.merge.RAW._lb0497._SFO-19._0001.1 \
-C "from AthenaCommon.AlgSequence import AthSequencer; from AthenaCommon.CFElements import findAlgorithm; findAlgorithm(AthSequencer('HLTBeginSeq'), 'L1TriggerByteStreamDecoder').MaybeMissingROBs+=[0x910081,0x910091];" \
--dump-config-exit \
TriggerJobOpts/runHLT_standalone.py \
> athenaHLT_pickle_gen.log 2>&1 


echo "Finished"
# ${SCRIPTSDIR}/extract-times.sh
# ${SCRIPTSDIR}/histmerge.sh >histmerge.log 2>&1

#echo "Finished post-processing for ${NFORKS}-${NTHREADS}-${NSLOTS} config"     

# --file /eos/atlas/atlastier0/rucio/data22_13p6TeV/physics_EnhancedBias/00440499/data22_13p6TeV.00440499.physics_EnhancedBias.merge.RAW/data22_13p6TeV.00440499.physics_EnhancedBias.merge.RAW._lb0497._SFO-11._0001.1 \
# --file /eos/atlas/atlastier0/rucio/data22_13p6TeV/physics_EnhancedBias/00440499/data22_13p6TeV.00440499.physics_EnhancedBias.merge.RAW/data22_13p6TeV.00440499.physics_EnhancedBias.merge.RAW._lb0497._SFO-12._0001.1 \
# --file /eos/atlas/atlastier0/rucio/data22_13p6TeV/physics_EnhancedBias/00440499/data22_13p6TeV.00440499.physics_EnhancedBias.merge.RAW/data22_13p6TeV.00440499.physics_EnhancedBias.merge.RAW._lb0497._SFO-13._0001.1 \
# --file /eos/atlas/atlastier0/rucio/data22_13p6TeV/physics_EnhancedBias/00440499/data22_13p6TeV.00440499.physics_EnhancedBias.merge.RAW/data22_13p6TeV.00440499.physics_EnhancedBias.merge.RAW._lb0497._SFO-14._0001.1 \
# --file /eos/atlas/atlastier0/rucio/data22_13p6TeV/physics_EnhancedBias/00440499/data22_13p6TeV.00440499.physics_EnhancedBias.merge.RAW/data22_13p6TeV.00440499.physics_EnhancedBias.merge.RAW._lb0497._SFO-15._0001.1 \
# --file /eos/atlas/atlastier0/rucio/data22_13p6TeV/physics_EnhancedBias/00440499/data22_13p6TeV.00440499.physics_EnhancedBias.merge.RAW/data22_13p6TeV.00440499.physics_EnhancedBias.merge.RAW._lb0497._SFO-16._0001.1 \
# --file /eos/atlas/atlastier0/rucio/data22_13p6TeV/physics_EnhancedBias/00440499/data22_13p6TeV.00440499.physics_EnhancedBias.merge.RAW/data22_13p6TeV.00440499.physics_EnhancedBias.merge.RAW._lb0497._SFO-17._0001.1 \
# --file /eos/atlas/atlastier0/rucio/data22_13p6TeV/physics_EnhancedBias/00440499/data22_13p6TeV.00440499.physics_EnhancedBias.merge.RAW/data22_13p6TeV.00440499.physics_EnhancedBias.merge.RAW._lb0497._SFO-18._0001.1 \
# --file /eos/atlas/atlastier0/rucio/data22_13p6TeV/physics_EnhancedBias/00440499/data22_13p6TeV.00440499.physics_EnhancedBias.merge.RAW/data22_13p6TeV.00440499.physics_EnhancedBias.merge.RAW._lb0497._SFO-19._0001.1 \
# --file /eos/atlas/atlastier0/rucio/data22_13p6TeV/physics_EnhancedBias/00440499/data22_13p6TeV.00440499.physics_EnhancedBias.merge.RAW/data22_13p6TeV.00440499.physics_EnhancedBias.merge.RAW._lb0497._SFO-20._0001.1 \
