#!/bin/bash
#counter=1
CONFIGDIR=/eos/user/s/swaterho/vtune_test/Run_5_pickletest/pickle_configs/
for config in "$CONFIGDIR"/*.pkl; do

    # ((counter++))
    # if [[ $counter -gt 1 ]]; then
    #     break
    # fi
    # Create a folder `for this script's output
    folder=$(basename "${config%.pkl}")  # Remove the ".sh" extension from the script name
    mkdir -p "$folder"
    pushd ${folder}
    cp /eos/user/s/swaterho/vtune_test/Run_5_pickletest/jsons/* .
    date
    hostname
    echo "Running athena with $config"
    time vtune -collect hotspots -strategy=:trace:trace,ld-linux.so.2:notrace:notrace,ld-2.12.so:notrace:notrace,ld-linux.so:notrace:notrace,ld-linux-x86-64.so.2:notrace:notrace  -- \
    athena.py "$config" \
    > athena.log 2>&1 
    popd

done
