import os
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
from atlasify import atlasify
import json

# Specify directories
data_dirs = ["/eos/user/s/swaterho/vtune_test/Run_28_newMenu_F1TxSx_smk3154_r220101_athHLT_pkl_lb497",
             "/eos/user/s/swaterho/vtune_test/Run_26_FxT1S1_smk3154_r220101_athHLT_pkl_lb497",
             "/eos/user/s/swaterho/vtune_test/Run_27_newMenu_FxT1Sx_smk3154_r220101_athHLT_pkl_lb497",
             "/eos/user/s/swaterho/vtune_test/Run_31_newMenu_FxT1Sx_RAFALmenu_r220101_athHLT_pkl_lb497",
             "/eos/user/s/swaterho/vtune_test/Run_32_newMenu_FxT1Sx_RAWdata_smk3154_r220101_athHLT_pkl_lb497"]

colors = ['r', 'b', 'g', 'y', 'm']  
markers = ['o', 'v', '^', 's', '*', '8', 's', 'p', 'h', 'H', 'D', 'd', 'P', 'X']
labels = ['MT', 'Pure MP', 'MP run menu, EB', 'MP Rafals menus', 'MP pure raw PHYS data']

fig, ax = plt.subplots(1, 1, figsize=(8, 8))  # Move this line up here

for i, data_dir in enumerate(data_dirs):
    x = []
    y = []

    # Change working directory to the current dataset directory
    os.chdir(data_dir)

    for dir in os.listdir("."):
        if not os.path.isdir(dir):
            continue

        if 'MP' in labels[i]: 
            x_value = int(dir.split("-")[0]) 
        else:
            x_value = int(dir.split("-")[1])


        # Skip odd x_value, but keep x_value = 1
        if x_value != 1 and x_value % 2 != 0:
            continue

        os.chdir(dir)

        if os.path.isfile("prmon.json"):
            with open("prmon.json", "r") as json_file:
                data = json.load(json_file)
            try:
                pss_kB = data['Max']['pss']
                pss_GB = pss_kB / 1000000  # convert from kB to GB
                #print(pss_GB)
            except KeyError:
                pss_GB = None  # or any other default value


            x.append(x_value)
            y.append(float(pss_GB))

        os.chdir("..")

    plt.scatter(x, y, edgecolors=colors[i], marker=markers[i], label=labels[i], s=50, facecolors='none')


    #os.chdir("..")



ax.set_xlabel("Number of events processed in parallel", loc='center', fontsize=15, labelpad=8)
ax.set_ylabel("Max PSS [GB]", loc='center', fontsize=15, labelpad=12)
#ax.set_xlim(0,42)
#ax.set_ylim(0,128)
ax.grid(axis='both', which='major', linestyle='-', alpha=0.7)
ax.grid(axis='both', which='minor', linestyle='-', alpha=0.3)
ax.xaxis.set_major_locator(ticker.MultipleLocator(8.0))
ax.yaxis.set_major_locator(ticker.MultipleLocator(10.0))

ax.xaxis.set_minor_locator(ticker.MultipleLocator(10))
ax.yaxis.set_minor_locator(ticker.MultipleLocator(5.0))

ax.set_title('Max PSS, new menu signatures (Hardware limit = 128 GB)')

ax.tick_params(axis='both', which='major', labelsize=14)

atlasify('Internal', 'Data, <$\mu$> $=$54')

fig.savefig("graph-pss.pdf")
print("Graph saved in", os.getcwd())