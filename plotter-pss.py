import os
import matplotlib.pyplot as plt
import atlasplots as aplt
import matplotlib.ticker as ticker
import json
from atlasify import atlasify

fig, ax = plt.subplots(1, 1)

x = []
y = []

# Change working directory to the current dataset directory
current_dir = os.getcwd()

for dir in os.listdir(current_dir):
    if not os.path.isdir(dir):
        continue

    os.chdir(dir)

    forks = dir.split("-")[0]
    threads = dir.split("-")[1]
    slots = dir.split("-")[2] 

    x_value = int(forks) * int(slots)

    if os.path.isfile("prmon.json"):
        with open("prmon.json", "r") as json_file:
            data = json.load(json_file)
        try:
            pss_kB = data['Max']['pss']
            pss_GB = pss_kB / 1000000  # convert from kB to GB
        except KeyError:
            pss_GB = None

        x.append(x_value)
        y.append(float(pss_GB))

    os.chdir("..")

plt.plot(x, y, 'bo')

# Add dashed red line at y=128
ax.axhline(y=128, color='red', linestyle='dashed', label='Hardware limit')

ax.set_xlabel("Number of events processed in parallel", loc='center', fontsize=15, labelpad=8)
ax.set_ylabel("Max PSS [GB]", loc='center', fontsize=15, labelpad=12)
ax.grid(axis='both', which='major', linestyle='-', alpha=0.7)
ax.grid(axis='both', which='minor', linestyle='-', alpha=0.3)

ax.set_xlim(0, 72)
ax.set_ylim(0, 130)
ax.xaxis.set_major_locator(ticker.MultipleLocator(8.0))
ax.yaxis.set_major_locator(ticker.MultipleLocator(10.0))
ax.xaxis.set_minor_locator(ticker.MultipleLocator(10))
ax.yaxis.set_minor_locator(ticker.MultipleLocator(5.0))

ax.set_title(f'Max PSS, F{forks} T{threads} S{slots}, pure EB, new menu')

ax.tick_params(axis='both', which='major', labelsize=14)

atlasify('Internal', 'Data, <$\mu$> $=$54')

fig.savefig("graph-pss.pdf")
print("Graph saved as graph-pss.pdf in", current_dir)
