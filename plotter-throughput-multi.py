import os
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
from atlasify import atlasify

data_dirs = ["/eos/user/s/swaterho/vtune_test/Run_28_newMenu_F1TxSx_smk3154_r220101_athHLT_pkl_lb497",
             "/eos/user/s/swaterho/vtune_test/Run_26_FxT1S1_smk3154_r220101_athHLT_pkl_lb497",
             "/eos/user/s/swaterho/vtune_test/Run_31_newMenu_FxT1Sx_RAFALmenu_r220101_athHLT_pkl_lb497",
             "/eos/user/s/swaterho/vtune_test/Run_32_newMenu_FxT1Sx_RAWdata_smk3154_r220101_athHLT_pkl_lb497"]

colors = ['r', 'b', 'g', 'y']
labels = ['Run 28', 'Run 26', 'Run 31', 'Run 32']

fig, ax = plt.subplots(1, 1)

for i, data_dir in enumerate(data_dirs):
    os.chdir(data_dir)
    x = []
    y = []

    for dir in os.listdir("."):
        if not os.path.isdir(dir):
            continue

        x_value = int(dir.split("-")[2])

        os.chdir(dir)

        if os.path.isfile("timestamps.txt"):
            with open("timestamps.txt", "r") as file:
                timestamps = [int(line.strip()) for line in file.readlines()[:2]]

            try:
                time_diff = 19210 / (timestamps[1] - timestamps[0])
                if time_diff < 0:
                    time_diff = 19210 / ((timestamps[1] + 86400) - timestamps[0])
            except ZeroDivisionError:
                time_diff = 0

            x.append(x_value)
            y.append(time_diff)

        os.chdir("..")

    plt.scatter(x, y, color=colors[i], label=labels[i])

ax.set_xlabel("Number of events processed in parallel", loc='center', fontsize=15, labelpad=8)
ax.set_ylabel("Throughput [events / s]", loc='center', fontsize=15, labelpad=12)
ax.set_xlim(0, 56)
ax.set_ylim(0, 80)
ax.grid(axis='both', which='major', linestyle='-', alpha=0.7)
ax.grid(axis='both', which='minor', linestyle='-', alpha=0.3)
ax.xaxis.set_major_locator(ticker.MultipleLocator(8.0))
ax.yaxis.set_major_locator(ticker.MultipleLocator(10.0))
ax.xaxis.set_minor_locator(ticker.MultipleLocator(10))
ax.yaxis.set_minor_locator(ticker.MultipleLocator(5.0))
ax.tick_params(axis='both', which='major', labelsize=14)
ax.set_title('Pure Multiprocessing Throughput, Pure Phys RAW data (forks=slots)')
atlasify('Internal', 'Data, <$\mu$> $=$54')

plt.legend()
fig.savefig("graph-throughput.pdf")
print("Plot saved as graph-throughput.pdf")