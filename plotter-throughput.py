# Will extract the times from each run and plot them, need to run 
# run as 'python plotter.py'
import os
import matplotlib.pyplot as plt
import atlasplots as aplt
import ROOT as root
import matplotlib.ticker as ticker
from atlasify import atlasify

SCRIPTSDIR = "/afs/cern.ch/user/s/swaterho/QT/scripts"

# Loop over all subdirectories, extracts the times and plots them
for dir in os.listdir("."):
    if not os.path.isdir(dir):
        continue

    # Extract the thread/forks value from the directory name
    forks = dir.split("-")[0]
    threads = dir.split("-")[1]
    slots = dir.split("-")[2] 

    x_value = int(forks) * int(slots)

    # Change to the current directory
    os.chdir(dir)

    # Check if timestamps.txt exists in this directory
    if os.path.isfile("timestamps.txt"):
        with open("timestamps.txt", "r") as file:
            # Read the first two lines of timestamps.txt
            timestamps = [int(line.strip()) for line in file.readlines()[:2]]

        # Calculate the throughput via (number of events)/(execution wall time), correcting for a run that passes over midnight, and handling ZeroDivisionError
        try:
            time_diff = 19210 / (timestamps[1] - timestamps[0])
            #print(time_diff)
            if time_diff < 0:
                time_diff = 19210 / ((timestamps[1] + 86400) - timestamps[0])
        except ZeroDivisionError:
            time_diff = 0

        with open("../results.txt", "a") as result_file:
            result_file.write("{} {}\n".format(x_value, time_diff))

        with open("../timediffs.txt", "a") as timediff_file:
            timediff_file.write("{} {}\n".format(x_value, time_diff))

    # Change back to the parent directory
    os.chdir("..")

# Plot the time differences using Matplotlib
x = []
y = []
timediffs_file = os.path.join(os.getcwd(), "timediffs.txt")

if os.path.isfile(timediffs_file):
    with open(timediffs_file, "r") as timediff_file:
        for line in timediff_file:
            x_value, time_diff = line.strip().split()
            x_value = int(x_value)  # convert to int before checking
            if x_value % 2 == 0 or x_value == 1:  # check if x_value is even or equals 1
                x.append(x_value)
                y.append(float(time_diff))


    fig, ax = plt.subplots(1, 1)

    plt.plot(x, y, 'ro')

    #ax.add_margins(top=0.18, left=0.05, right=0.05, bottom=0.05)
    ax.set_xlabel("Number of events processed in parallel", loc='center', fontsize=15, labelpad=8)
    ax.set_ylabel("Throughput [events / s]", loc='center', fontsize=15, labelpad=12)
    ax.set_xlim(0,70)
    ax.set_ylim(0,80)
    #ax.set_title('')
    ax.grid(axis='both', which='major', linestyle='-', alpha=0.7)
    ax.grid(axis='both', which='minor', linestyle='-', alpha=0.3)
    ax.xaxis.set_major_locator(ticker.MultipleLocator(8.0))
    ax.yaxis.set_major_locator(ticker.MultipleLocator(10.0))

    ax.xaxis.set_minor_locator(ticker.MultipleLocator(10))
    ax.yaxis.set_minor_locator(ticker.MultipleLocator(5.0))
    
    ax.tick_params(axis='both', which='major', labelsize=14)
    #ax.tick_params(axis='both', which='minor', labelsize=8) 

    ax.set_title(f'Throughput, F{forks} T{threads} S{slots}, pure EB, new menu')

    atlasify('Internal', 'Data, <$\mu$> $=$54')

    fig.savefig("graph-throughput.pdf")

    os.remove("results.txt")
    os.remove("timediffs.txt")

    print("Plot saved as graph-throughput.pdf")
else:
    print("No timediffs.txt file found.")
