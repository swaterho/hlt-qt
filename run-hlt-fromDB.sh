NFORKS=$1            #number of forks on a TPU (--nproc in athenaHLT)
NTHREADS=$2        #number of threads in each fork (--nthreads)
NSLOTS=$3  #number of event slots in each fork (--concurrent-events)
NEVENTS=10
SCRIPTSDIR=/afs/cern.ch/user/s/swaterho/QT/dev/scripts

#echo "Copying configuration files"
#cp ../config/*.json .
#cp ../../test/AthHLT.sor.pkl .
date
echo "Running athenaHLT with ${NFORKS}-${NTHREADS}-${NSLOTS} config"
#vtune -collect hotspots -strategy=:trace:trace,ld-linux.so.2:notrace:notrace,ld-2.12.so:notrace:notrace,ld-linux.so:notrace:notrace,ld-linux-x86-64.so.2:notrace:notrace -- \
prmon -- athenaHLT.py \
--perfmon --dump-config \
--threads=${NTHREADS}  --concurrent-events=${NSLOTS} --evtMax=${NEVENTS} \
-b --db-server TRIGGERDB_RUN3 --smk 3154 --l1psk 3940 --hltpsk 3898 \
-R 440499 -L 497 \
-f /eos/atlas/atlastier0/rucio/data22_13p6TeV/physics_EnhancedBias/00440499/data22_13p6TeV.00440499.physics_EnhancedBias.merge.RAW/data22_13p6TeV.00440499.physics_EnhancedBias.merge.RAW._lb0497._SFO-11._0001.1 \
-f /eos/atlas/atlastier0/rucio/data22_13p6TeV/physics_EnhancedBias/00440499/data22_13p6TeV.00440499.physics_EnhancedBias.merge.RAW/data22_13p6TeV.00440499.physics_EnhancedBias.merge.RAW._lb0497._SFO-12._0001.1 \
-f /eos/atlas/atlastier0/rucio/data22_13p6TeV/physics_EnhancedBias/00440499/data22_13p6TeV.00440499.physics_EnhancedBias.merge.RAW/data22_13p6TeV.00440499.physics_EnhancedBias.merge.RAW._lb0497._SFO-13._0001.1 \
-f /eos/atlas/atlastier0/rucio/data22_13p6TeV/physics_EnhancedBias/00440499/data22_13p6TeV.00440499.physics_EnhancedBias.merge.RAW/data22_13p6TeV.00440499.physics_EnhancedBias.merge.RAW._lb0497._SFO-14._0001.1 \
-f /eos/atlas/atlastier0/rucio/data22_13p6TeV/physics_EnhancedBias/00440499/data22_13p6TeV.00440499.physics_EnhancedBias.merge.RAW/data22_13p6TeV.00440499.physics_EnhancedBias.merge.RAW._lb0497._SFO-15._0001.1 \
-f /eos/atlas/atlastier0/rucio/data22_13p6TeV/physics_EnhancedBias/00440499/data22_13p6TeV.00440499.physics_EnhancedBias.merge.RAW/data22_13p6TeV.00440499.physics_EnhancedBias.merge.RAW._lb0497._SFO-16._0001.1 \
-f /eos/atlas/atlastier0/rucio/data22_13p6TeV/physics_EnhancedBias/00440499/data22_13p6TeV.00440499.physics_EnhancedBias.merge.RAW/data22_13p6TeV.00440499.physics_EnhancedBias.merge.RAW._lb0497._SFO-17._0001.1 \
-f /eos/atlas/atlastier0/rucio/data22_13p6TeV/physics_EnhancedBias/00440499/data22_13p6TeV.00440499.physics_EnhancedBias.merge.RAW/data22_13p6TeV.00440499.physics_EnhancedBias.merge.RAW._lb0497._SFO-18._0001.1 \
-f /eos/atlas/atlastier0/rucio/data22_13p6TeV/physics_EnhancedBias/00440499/data22_13p6TeV.00440499.physics_EnhancedBias.merge.RAW/data22_13p6TeV.00440499.physics_EnhancedBias.merge.RAW._lb0497._SFO-19._0001.1 \
-f /eos/atlas/atlastier0/rucio/data22_13p6TeV/physics_EnhancedBias/00440499/data22_13p6TeV.00440499.physics_EnhancedBias.merge.RAW/data22_13p6TeV.00440499.physics_EnhancedBias.merge.RAW._lb0497._SFO-20._0001.1 \
>athenaHLT.log 2>&1 


echo "Finished, running post-processing for ${NFORKS}-${NTHREADS}-${NSLOTS} config"
# ${SCRIPTSDIR}/extract-times.sh
# ${SCRIPTSDIR}/histmerge.sh >histmerge.log 2>&1

#echo "Finished post-processing for ${NFORKS}-${NTHREADS}-${NSLOTS} config"     