#!/bin/bash
#forkConfigs=( 32 30 28 26 24 22 20 18 16 14 12 10 8 6 4 2 1 )
threadConfigs=( 1 2 4 6 8 10 12 14 16 18 20 22 24 26 28 30 32 )
slotConfigs=( 1 2 4 6 8 10 12 14 16 18 20 22 24 26 28 30 32 )

SCRIPTSDIR=/afs/cern.ch/user/s/swaterho/QT/scripts

# Single fork, multithreaded, single slot
nF=1
# mkdir -p nF1_nTx_nSx
# cd nF1_nTx_nSx

for nT in "${threadConfigs[@]}"; do
  confName="${nF}-${nT}-${nT}"
  mkdir -p ${confName}
  pushd ${confName}
  time ${SCRIPTSDIR}/pickle_config_gen_hlt.sh ${nF} ${nT} ${nT} \
  wait
  popd
done

echo "Finished running"