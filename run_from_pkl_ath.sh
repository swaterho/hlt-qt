#!/bin/bash
    
# Loop over all subdirectories
for dir in */; do
    # Change to the current directory
    cd "$dir"
    date
    hostname
    time athena.py config"$(basename "$(pwd)")".pkl &> athena.log &
    wait
    # Change back to the parent directory
    cd ..
done
