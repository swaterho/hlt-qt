#!/bin/bash
    
# Loop over all subdirectories
for dir in */; do
    # Change to the current directory
    cd "$dir"
    date
    hostname
    time athenaHLT.py config"$(basename "$(pwd)")".pkl &> athenaHLT.log &
    wait
    # Change back to the parent directory
    cd ..
done
